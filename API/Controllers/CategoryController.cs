﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Libraries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Service;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Produces("application/json")]
    [Route("api/category")]
    [Authorize(Roles = "User")]
    public class CategoryController : Controller
    {
        private readonly IServiceManager _serviceManager;

        public CategoryController(IServiceManager serviceManager)
        {
            _serviceManager = serviceManager;
        }

        [HttpGet]
        public JsonResult List()
        {
            try
            {
                _serviceManager.CategoryService.setRequest(Request);
                return Json(_serviceManager.CategoryService.List());
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }

        [HttpPost]
        public JsonResult Add([BindRequired, FromBody] CategoryDTO category)
        {
            try
            {
                _serviceManager.CategoryService.setRequest(Request);
                var isCategoryPresent = _serviceManager.CategoryService.GetByName(category.name);
                if (isCategoryPresent)
                {
                    ModelState.AddModelError("name", "Please type in another category");
                }

                if (ModelState.IsValid)
                {
                    return Json(_serviceManager.CategoryService.Add(category));
                }
                else
                {
                    Response.StatusCode = 400;
                    return Json(ModelErrors.Generate(ModelState));
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }

        [HttpPut("{id}")]
        public JsonResult Update(string id, [FromBody] CategoryDTO category)
        {
            //this logic might needs to be redone
            try
            {
                _serviceManager.CategoryService.setRequest(Request);
                var duplicateCategory = _serviceManager.CategoryService.GetByName(id, category.name);
                var currentCategoryCheck = _serviceManager.CategoryService.Get(id);
                if (duplicateCategory)
                {
                    ModelState.AddModelError("name", "Please choose another category");
                }
                //why is this here?
                //if (currentCategoryCheck == null)
                //{
                //    ModelState.AddModelError("name", "Error trying to save category");
                //}

                if (ModelState.IsValid)
                {
                    category.id = id;
                    return Json(_serviceManager.CategoryService.Update(category));
                }
                else
                {
                    Response.StatusCode = 400;
                    return Json(ModelErrors.Generate(ModelState));
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }

        [HttpDelete("{id}")]
        public JsonResult Delete(string id)
        {
            _serviceManager.CategoryService.setRequest(Request);
            var currentCategory = _serviceManager.CategoryService.Get(id);
            if (currentCategory == null)
            {
                ModelState.AddModelError("name", "Error trying to remove category");
            }

            if (ModelState.IsValid)
            {
                _serviceManager.CategoryService.Remove(id);
                return Json(new { success = true });
            }
            else
            {
                Response.StatusCode = 400;

                return Json(ModelErrors.Generate(ModelState));
            }
        }

    }
}

