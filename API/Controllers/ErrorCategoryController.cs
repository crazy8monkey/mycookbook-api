﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Libraries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Service;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Produces("application/json")]
    [Route("api/error-category")]
    public class ErrorCategoryController : Controller
    {
        private readonly IServiceManager _serviceManager;

        public ErrorCategoryController(IServiceManager serviceManager)
        {
            _serviceManager = serviceManager;
        }

        [Authorize(Roles = "Admin, User")]
        [HttpGet]
        public JsonResult List()
        {
            try
            {
                return Json(_serviceManager.ErrorCategoryService.List());
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public JsonResult Add([FromBody] ErrorCategoryDTO category)
        {
            try
            {
                Boolean isCategoryPresent = _serviceManager.ErrorCategoryService.getByName(category.name);
                if (isCategoryPresent)
                {
                    ModelState.AddModelError("name", "Please type in another category");
                }

                if (ModelState.IsValid)
                {
                    return Json(_serviceManager.ErrorCategoryService.Add(category));
                }
                else
                {
                    Response.StatusCode = 400;
                    return Json(ModelErrors.Generate(ModelState));
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("{id}")]
        public JsonResult Get(string id) {
            try
            {
                return Json(_serviceManager.ErrorCategoryService.Get(id));
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("{id}")]
        public JsonResult Update(int id, [FromBody] ErrorCategoryDTO category)
        {
            try
            {
                return Json(_serviceManager.ErrorCategoryService.Update(category));
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public JsonResult Remove(string id)
        {
            try
            {
                _serviceManager.ErrorCategoryService.Remove(id);
                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }
    }
}




