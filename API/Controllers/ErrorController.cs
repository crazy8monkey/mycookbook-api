﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Libraries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Service;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Produces("application/json")]
    [Route("api/error")]
    public class ErrorController : Controller
    {

        private readonly IServiceManager _serviceManager;

        public ErrorController(IServiceManager serviceManager)
        {
            _serviceManager = serviceManager;
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public JsonResult List()
        {
            try
            {
                return Json(_serviceManager.ErrorService.List());
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("{id}")]
        public JsonResult Get(string id)
        {
            try
            {
                return Json(_serviceManager.ErrorService.Get(id));
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }

        [Authorize(Roles = "User")]
        [HttpPost]
        public JsonResult Add([BindRequired, FromBody] ErrorDTO newError)
        {
            try
            {
                _serviceManager.ErrorService.setRequest(Request);
                if (_serviceManager.ErrorService.getUser() == 0)
                {

                    if (newError.first_name == "")
                    {
                        ModelState.AddModelError("first_name", "First Name is required");
                    }

                    if (newError.last_name == "")
                    {
                        ModelState.AddModelError("last_name", "First Name is required");
                    }
                }

                if(newError.description == "")
                {
                    ModelState.AddModelError("description", "Description is required");
                }

                if (ModelState.IsValid)
                {
                    newError.category = "0";
                    _serviceManager.ErrorService.Add(newError);
                    return Json(new { success = true });
                }
                else
                {
                    Response.StatusCode = 400;
                    return Json(ModelErrors.Generate(ModelState));
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));

            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("{id}")]
        public JsonResult Update(string id, [FromBody] ErrorDTO updateError)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    updateError.id = id;
                    return Json(_serviceManager.ErrorService.Update(updateError));
                }
                else
                {
                    Response.StatusCode = 400;
                    return Json(ModelErrors.Generate(ModelState));
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public JsonResult Remove(string id)
        {
            try
            {
                _serviceManager.ErrorService.Remove(id);
                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }

    }
}
