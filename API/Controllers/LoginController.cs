﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Libraries;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Service;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Produces("application/json")]
    [Route("api/login")]
    public class LoginController : Controller
    {
        private readonly IServiceManager _serviceManager;

        public LoginController(IServiceManager serviceManager)
        {
            _serviceManager = serviceManager;
        }

        [HttpPost]
        public JsonResult Index([BindRequired, FromBody] LoginDTO login)
        {
            try
            {
                var checkUser = _serviceManager.LoginService.ValidateCredentials(login);

                if (!checkUser && login.email != "" && login.password != "")
                {
                    ModelState.AddModelError("form", "Informaton does not match to a registered account");
                }

                if (ModelState.IsValid)
                {
                    UserDTO loggedInUser = _serviceManager.LoginService.LoginUser(login);
                    return Json(new
                    {
                        token = _serviceManager.TokenService.GenerateToken(loggedInUser.id, loggedInUser.user_role),
                        refresh_token = loggedInUser.refresh_token,
                        user = new
                        {
                            first_name = loggedInUser.first_name,
                            last_name = loggedInUser.last_name,
                            email = loggedInUser.email,
                            profile = loggedInUser.profile_pic,
                            profile_type = loggedInUser.profile_pic_type
                        }
                    });
                }
                else
                {
                    Response.StatusCode = 400;
                    return Json(ModelErrors.Generate(ModelState));
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }
    }
}
