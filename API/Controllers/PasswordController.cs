﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Libraries;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Service;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Produces("application/json")]
    [Route("api/password")]
    public class PasswordController : Controller
    {
        private readonly IServiceManager _serviceManager;

        public PasswordController(IServiceManager serviceManager)
        {
            _serviceManager = serviceManager;
        }

        [HttpPut("new")]
        public JsonResult ResetPassword([BindRequired, FromBody] NewPasswordDTO credentials)
        {
            try
            {
                if (credentials.password != null && credentials.password.Length < 6)
                {
                    ModelState.AddModelError("password", "Password needs to be at least 6 characters");
                }

                if (credentials.password != null && credentials.retype_password != null && (credentials.password != credentials.retype_password))
                {
                    ModelState.AddModelError("retype_password", "Passwords do not match");
                }

                if (ModelState.IsValid)
                {
                    _serviceManager.PasswordService.UpdatePassword(credentials);
                    return Json(new
                    {
                        success = true
                    });
                }
                else
                {
                    Response.StatusCode = 400;
                    return Json(ModelErrors.Generate(ModelState));
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }

        [HttpPost("reset")]
        public JsonResult Retrieve([BindRequired, FromBody] ResetPasswordDTO reset)
        {
            try
            {
                UserDTO currentUser = _serviceManager.PasswordService.GetByEmail(reset.email, reset.role);
                if (currentUser == null)
                {
                    ModelState.AddModelError("email", "Email is not associated to an account");
                }

                if (ModelState.IsValid)
                {
                    return Json(new
                    {
                        user_id = currentUser.id,
                        reset_password = _serviceManager.PasswordService.ResetPassword(currentUser.id)
                    });
                }
                else
                {
                    Response.StatusCode = 400;
                    return Json(ModelErrors.Generate(ModelState));
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }
    }
}
