﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Libraries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Service;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Produces("application/json")]
    [Route("api/profile")]
    [Authorize(Roles = "User")]
    public class ProfileController : Controller
    {
        private readonly IServiceManager _serviceManager;
        
        public ProfileController(IServiceManager serviceManager)
        {
            _serviceManager = serviceManager;
        }

        [HttpGet]
        public JsonResult Get()
        {
            try
            {
                _serviceManager.UserService.setRequest(Request);
                return Json(_serviceManager.ProfileService.GetSessionUser());
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }

        [HttpPut]
        public JsonResult Update([BindRequired, FromBody] ProfileDTO profile)
        {
            try
            {
                if (profile.new_password != "" && profile.new_password.Length < 6)
                {
                    ModelState.AddModelError("new_password", "Password needs to be at least 6 characters");
                }

                if (profile.new_password != "" && profile.confirm_password != "" && (profile.new_password != profile.confirm_password))
                {
                    ModelState.AddModelError("confirm_password", "Passwords do not match");
                }

                if (ModelState.IsValid)
                {
                    _serviceManager.ProfileService.setRequest(Request);
                    _serviceManager.ProfileService.Update(profile);
                    return Json(new
                    {
                        success = true
                    });
                }
                else
                {
                    Response.StatusCode = 400;
                    return Json(ModelErrors.Generate(ModelState));
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }

        [Authorize]
        [HttpPost("picture")]
        public JsonResult Picture([BindRequired, FromForm] ProfileImageDTO image)
        {
            try
            {
                _serviceManager.ProfileService.setRequest(Request);
                ImageDTO newProfile = _serviceManager.ProfileService.NewImage(image);
                return Json(new
                {
                    profile_pic = newProfile.picture,
                    profile_pic_type = newProfile.type
                });
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }
    }
}