﻿// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

using System;
using Contracts;
using Libraries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using Service;

namespace API.Controllers
{
    [Produces("application/json")]
    [Route("api/recipes")]
    [Authorize(Roles = "User")]
    public class RecipeController : Controller
    {
        private readonly IServiceManager _serviceManager;
        
        public RecipeController(IServiceManager serviceManager) {
            _serviceManager = serviceManager;
        }

        [HttpGet]
        public JsonResult List()
        {
            try
            {
                _serviceManager.RecipeService.setRequest(Request);
                return Json(_serviceManager.RecipeService.List());
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }

        [HttpPost]
        public JsonResult Add([FromBody] RecipeDTO recipe)
        {
            try
            {
                _serviceManager.RecipeService.setRequest(Request);
                if (ModelState.IsValid)
                {
                    return Json(_serviceManager.RecipeService.Add(recipe));
                }
                else
                {
                    Response.StatusCode = 400;
                    return Json(ModelErrors.Generate(ModelState));
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }


        [HttpGet("{id}")]
        public JsonResult Get(string id)
        {
            try
            {
                _serviceManager.RecipeService.setRequest(Request);
                var currentRecipe = _serviceManager.RecipeService.Get(id);
                if (currentRecipe == null)
                {
                    ModelState.AddModelError(string.Empty, "There was an error grabbing entry");
                }

                if (ModelState.IsValid)
                {
                    return Json(currentRecipe);
                }
                else
                {
                    Response.StatusCode = 400;
                    return Json(ModelErrors.Generate(ModelState));
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }

        [HttpPut("{id}")]
        public JsonResult Update(string id, [FromBody] RecipeDTO recipe)
        {
            _serviceManager.RecipeService.setRequest(Request);
            var currentRecipe = _serviceManager.RecipeService.Get(id);

            if (currentRecipe == null)
            {
                ModelState.AddModelError("name", "There is an error saving this recipe");
            }

            if (ModelState.IsValid)
            {
                recipe.id = id;
                return Json(_serviceManager.RecipeService.Update(recipe));
            }
            else
            {
                Response.StatusCode = 400;
                return Json(ModelErrors.Generate(ModelState));
            }
        }

        [HttpDelete("{id}")]
        public JsonResult Remove(string id)
        {
            try
            {
                _serviceManager.RecipeService.setRequest(Request);
                var currentRecipe = _serviceManager.RecipeService.Get(id);
                if (currentRecipe == null)
                {
                    ModelState.AddModelError(string.Empty, "There is an error removing this recipe");
                }

                if (ModelState.IsValid)
                {
                    _serviceManager.RecipeService.Remove(id);
                    return Json(new { success = true });
                }
                else
                {
                    Response.StatusCode = 400;
                    return Json(ModelErrors.Generate(ModelState));
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }
    }
}





