﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Libraries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Service;

//https://sankhadip.medium.com/how-to-upload-files-in-net-core-web-api-and-react-36a8fbf5c9e8

namespace API.Controllers
{
    [Produces("application/json")]
    [Route("api/recipe-image")]
    [Authorize(Roles = "User")]
    public class RecipeImageController : Controller
    {
        private readonly IServiceManager _serviceManager;

        public RecipeImageController(IServiceManager serviceManager)
        {
            _serviceManager = serviceManager;
        }

        [HttpGet("{id}")]
        public JsonResult List(string id)
        {
            try
            {
                return Json(_serviceManager.RecipeImageService.List(id));
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }

        [HttpPost]
        public JsonResult Add([FromForm] RecipeImageUploadDTO images)
        {
            try
            {
                return Json(_serviceManager.RecipeImageService.Add(images));
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }

        [HttpPut]
        public JsonResult Update([FromForm] RecipeImageUploadDTO images)
        {
            try
            {
                return Json(_serviceManager.RecipeImageService.Update(images));
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }

        }
    }
}



