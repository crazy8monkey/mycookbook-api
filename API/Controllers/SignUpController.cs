﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Libraries;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Service;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Produces("application/json")]
    [Route("api/signup")]
    public class SignUpController : Controller
    {
        private readonly IServiceManager _serviceManager;

        public SignUpController(IServiceManager serviceManager)
        {
            _serviceManager = serviceManager;
        }

        [HttpPost]
        public JsonResult Index([BindRequired, FromBody] UserDTO newUser)
        {
            try
            {
                var emailPresentCheck = _serviceManager.SignupService.EmailRegistered(newUser.email, newUser.user_role);
                if (emailPresentCheck)
                {
                    ModelState.AddModelError("email", "Email is already registered to another account");
                }
                if (newUser.new_password != null && newUser.new_password.Length < 6)
                {
                    ModelState.AddModelError("new_password", "Password needs to be at least 6 characters");
                }

                if (ModelState.IsValid)
                {
                    newUser.user_role = "User";
                    UserDTO newUserData = _serviceManager.UserService.Add(newUser);
                    return Json(new
                    {
                        token = _serviceManager.TokenService.GenerateToken(newUserData.id, newUserData.user_role),
                        refresh_token = newUserData.refresh_token,
                        user = new
                        {
                            first_name = newUserData.first_name,
                            last_name = newUserData.last_name,
                            email = newUserData.email,
                            profile = newUserData.profile_pic,
                            profile_type = newUserData.profile_pic_type
                        }
                    });
                }
                else
                {
                    Response.StatusCode = 400;
                    return Json(ModelErrors.Generate(ModelState));
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }

    }
}
