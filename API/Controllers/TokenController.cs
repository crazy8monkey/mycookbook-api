﻿using System;
using Contracts;
using Libraries;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Service;

namespace API.Controllers
{
    [Produces("application/json")]
    [Route("api/token")]
    public class TokenController : Controller
    {
        private readonly IServiceManager _serviceManager;
        
        public TokenController(IServiceManager serviceManager)
        {
            _serviceManager = serviceManager;
        }

        [HttpPost]
        public JsonResult Index([FromBody] RefreshTokenDTO refreshToken)
        {
            try
            {
                _serviceManager.TokenService.setRequest(Request);
                if (!_serviceManager.TokenService.ValidToken(refreshToken))
                {
                    Response.StatusCode = 400;
                    return Json(new { message = "Invalid token" });
                }

                return Json(_serviceManager.TokenService.NewToken(refreshToken));
            } catch(Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }       
    }
}


