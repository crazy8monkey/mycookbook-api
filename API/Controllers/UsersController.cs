﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Service;
using Libraries;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Produces("application/json")]
    [Route("api/users")]
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        private readonly IServiceManager _serviceManager;

        public UsersController(IServiceManager serviceManager)
        {
            _serviceManager = serviceManager;
        }

        [HttpGet]
        public JsonResult List()
        {
            try
            {
                return Json(_serviceManager.UserService.List());
            } catch(Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }

        [HttpGet("{id}")]
        public JsonResult Get(string id)
        {
            try
            {
                return Json(_serviceManager.UserService.Get(id));
            } catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }

        [HttpDelete("{id}")]
        public JsonResult Remove(string id)
        {
            try
            {
                _serviceManager.UserService.Remove(id);
                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(ExceptionHandler.Generate(ex));
            }
        }
    }
}
