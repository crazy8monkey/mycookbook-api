﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Contracts
{
    public class CategoryDTO : BaseDTO
    {
        [Required(ErrorMessage = "Name is Required")]
        public string name { get; set; }
    }
}
