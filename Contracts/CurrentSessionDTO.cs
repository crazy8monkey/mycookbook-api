﻿using System;
namespace Contracts
{
    public class CurrentSessionDTO
    {
        public long user { get; set; }
        public string role { get; set; }
    }
}
