﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Contracts
{
    public class ErrorDTO : BaseDTO
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public Nullable<long> user_id { get; set; }
        public string description { get; set; }
        public string category { get; set; }
        public string submitted_date { get; set; }
    }
}
