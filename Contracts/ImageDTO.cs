﻿using System;
namespace Contracts
{
    public class ImageDTO
    {
        public byte[] picture { get; set; }
        public string type { get; set; }
    }
}
