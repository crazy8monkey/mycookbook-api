﻿using System;
using Microsoft.AspNetCore.Http;

namespace Contracts
{
    public class ImageUploadDTO
    {
        public string imageName { get; set; }
        public IFormFile imageFile { get; set; }
    }
}
