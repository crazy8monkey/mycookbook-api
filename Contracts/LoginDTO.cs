﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Contracts
{
    public class LoginDTO
    {
        [Required(ErrorMessage = "Email is Required")]
        public string email { get; set; }

        [Required(ErrorMessage = "Password is Required")]
        public string password { get; set; }

        public string role { get; set; }
    }
}
