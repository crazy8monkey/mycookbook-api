﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Contracts
{
    public class NewPasswordDTO
    {
        [Required(ErrorMessage = "Password is Required")]
        public string password { get; set; }

        [Required(ErrorMessage = "Password is Required")]
        public string retype_password { get; set; }

        public string user_id { get; set; }
    }
}
