﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Contracts
{
    public class ProfileDTO
    {
        [Required(ErrorMessage = "First Name is Required")]
        public string first_name { get; set; }

        [Required(ErrorMessage = "Last Name is Required")]
        public string last_name { get; set; }

        [Required(ErrorMessage = "Email is Required")]
        [EmailAddress]
        public string email { get; set; }

        public string new_password { get; set; }
        public string confirm_password { get; set; }
    }
}
