﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.InteropServices;

namespace Contracts
{
    public class RecipeDTO : BaseDTO
    {
        [Required(ErrorMessage = "Name is Required")]
        public string name { get; set; }

        [Required(ErrorMessage = "Ingredients is Required")]
        public string ingredients { get; set; }

        [Required(ErrorMessage = "Instructions is Required")]
        public string instructions { get; set; }
        public string category { get; set; }

        public ImageDTO image { get; set; }
    }

    public class AddUpdateRecipeDTO : BaseDTO
    {
        
        public string name { get; set; }

        
        public string ingredients { get; set; }

        
        public string instructions { get; set; }
        public string category { get; set; }
    }
}
