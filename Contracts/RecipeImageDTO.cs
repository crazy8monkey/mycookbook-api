﻿using System;

namespace Contracts
{
    public class RecipeImageDTO : BaseDTO
    {
        public string name { get; set; }
        public byte[] picture { get; set; }
        public string picture_type { get; set; }
        public string recipe { get; set; }
        public Boolean is_primary { get; set; }
        public long size { get; set; }
    }
}
