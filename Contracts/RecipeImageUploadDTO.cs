﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace Contracts
{
    public class RecipeImageUploadDTO
    {
        public string recipe { get; set; }
        public List<IFormFile> imageFiles { get; set; }
    }
}
