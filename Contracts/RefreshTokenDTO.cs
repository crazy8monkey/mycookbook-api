﻿using System;
namespace Contracts
{
    public class RefreshTokenDTO
    {
        public string refresh_token { get; set; }
    }
}
