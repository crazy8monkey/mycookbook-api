﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Contracts
{
    public class ResetPasswordDTO
    {
        [EmailAddress]
        [Required(ErrorMessage = "Email is Required")]
        public string email { get; set; }

        public string role { get; set; }
    }
}
