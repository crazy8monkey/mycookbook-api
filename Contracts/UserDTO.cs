﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Contracts
{
    public class UserDTO : BaseDTO
    {
        [Required(ErrorMessage = "First Name is Required")]
        public string first_name { get; set; }

        [Required(ErrorMessage = "Last Name is Required")]
        public string last_name { get; set; }

        [Required(ErrorMessage = "Email is Required")]
        [EmailAddress]
        public string email { get; set; }

        [NotMapped]
        [Required(ErrorMessage = "Password is Required")]
        public string new_password { get; set; }

        [NotMapped]
        [Required(ErrorMessage = "Confirm Password is Required")]
        [Compare(nameof(new_password), ErrorMessage = "Passwords don't match.")]
        public string confirm_password { get; set; }

        public string refresh_token { get; set; }
        public Nullable<DateTime> reset_password_date { get; set; }

        public byte[] profile_pic { get; set; }
        public string profile_pic_type { get; set; }

        public string registered_date { get; set; }
        public string user_role { get; set; }
    }
}
