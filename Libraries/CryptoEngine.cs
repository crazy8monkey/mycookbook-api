﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Libraries
{
    public class CryptoEngine
    {
        public static string Encrypt(long input)
        {
            byte[] inputArray = UTF8Encoding.UTF8.GetBytes(input.ToString());
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes("sblw-3hn8-sqoy19");
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length).Replace("/", "_");
        }
        public static long Decrypt(string input)
        {
            byte[] inputArray = Convert.FromBase64String(input.Replace("_", "/"));
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes("sblw-3hn8-sqoy19");
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return Convert.ToInt64(UTF8Encoding.UTF8.GetString(resultArray));
        }

        public static byte[] GenerateSalt()
        {
            using (RNGCryptoServiceProvider saltGenerator = new RNGCryptoServiceProvider())
            {
                byte[] salt = new byte[32];
                saltGenerator.GetBytes(salt);
                return salt;
            }
        }

        public static byte[] ComputeHash(string password, byte[] salt)
        {
            using (Rfc2898DeriveBytes hashGenerator = new Rfc2898DeriveBytes(password, salt))
            {
                hashGenerator.IterationCount = 10101;
                return hashGenerator.GetBytes(32);
            }
        }
    }
}
