﻿using System;
namespace Libraries
{
    public class ExceptionHandler
    {
        public ExceptionHandler()
        {
        }

        public static dynamic Generate(Exception ex)
        {
            return new
            {
                msg = ex.Message,
                source = ex.Source,
                stackTrace = ex.StackTrace.Replace("\n", Environment.NewLine),
                data = ex.Data
            };
        }
    }
}
