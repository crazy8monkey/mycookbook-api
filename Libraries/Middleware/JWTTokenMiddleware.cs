﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using Contracts;

namespace Libraries.Middleware
{
    //https://faun.pub/asp-net-core-jwt-authentication-middleware-reading-a-jwt-14cdb32e39ca
    public class JWTTokenMiddleware
    {
        private readonly RequestDelegate _next;

        public JWTTokenMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            CurrentSessionDTO currentSession = new CurrentSessionDTO();
            string authHeader = context.Request.Headers[HeaderNames.Authorization];
            if (authHeader != null) {
                var jwtTokenHandler = new JwtSecurityTokenHandler();
                string accessBearerToken = authHeader.Replace("Bearer ", "");
                JwtSecurityToken jwtToken = jwtTokenHandler.ReadToken(accessBearerToken) as JwtSecurityToken;
                string userRequested = jwtToken.Claims.Where(p => p.Type == ClaimTypes.NameIdentifier).FirstOrDefault()?.Value;
                string userRole = jwtToken.Claims.Where(p => p.Type == ClaimTypes.Role).FirstOrDefault()?.Value;
                currentSession.user = CryptoEngine.Decrypt(userRequested);
                currentSession.role = userRole;
            } else
            {
                currentSession.user = 0;
            }
            
            context.Features.Set<CurrentSessionDTO>(currentSession);

            //Pass to the next middleware
            await _next(context);
        }
    }
}
