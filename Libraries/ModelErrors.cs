﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Libraries
{
    public class ModelErrors
    {
        public ModelErrors()
        {
        }

        public static dynamic Generate(ModelStateDictionary modelState)
        {
            var errors = new ExpandoObject() as IDictionary<string, object>;
            foreach (var key in modelState.Keys)
            {
                List<string> keyMessages = new List<string>();
                foreach (var error in modelState[key].Errors)
                {
                    keyMessages.Add(error.ErrorMessage);
                }

                errors.Add(key, keyMessages);
            }

            return errors;
        }
    }
}
