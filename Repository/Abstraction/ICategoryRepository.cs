﻿using System;
using System.Linq.Expressions;
using Domain.Entities;
using Domain.Repositories;

namespace Domain.Abstraction
{
    public interface ICategoryRepository : IRepository<Category>
    {
    }
}
