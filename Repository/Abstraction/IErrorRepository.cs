﻿using System;
using System.Collections.Generic;
using Domain.Entities;
using Domain.Repositories;

namespace Domain.Abstraction
{
    public interface IErrorRepository : IRepository<Error>
    {
        void UpdateCategories(long current_category, long new_category);
    }
}
