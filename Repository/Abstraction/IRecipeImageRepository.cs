﻿using System;
using System.Collections.Generic;
using Domain.Entities;
using Domain.Repositories;

namespace Domain.Abstraction
{
    public interface IRecipeImageRepository : IRepository<RecipeImage>
    {
        List<RecipeImage> Add(List<RecipeImage> images);
        List<RecipeImage> Update(long recipe_id, List<RecipeImage> add);

        RecipeImage getMainImage(long recipe_id);
        void RemoveByRecipe(long recipe_id);
    }
}
