﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities;

namespace Domain.Repositories
{
    public interface IRecipeRepository : IRepository<Recipe>
    {
        void UpdateCategories(Recipe recipe);
    }
}
