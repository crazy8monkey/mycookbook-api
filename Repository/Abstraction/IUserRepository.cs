﻿using System;
using System.Linq.Expressions;
using Domain.Abstraction;
using Domain.Entities;
using Domain.Repositories;

namespace Domain.Abstraction
{
    public interface IUserRepository : IRepository<User>
    {
    }
}
