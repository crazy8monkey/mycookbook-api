﻿using System;

namespace Domain.Entities
{
    public class Category : BaseEntity
    {
        public string name { get; set; }
        public long user_id { get; set; }
    }
}
