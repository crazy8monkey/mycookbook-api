﻿using System;
namespace Domain.Entities
{
    public class Error : BaseEntity
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public Nullable<long> user_id { get; set; }
        public string description { get; set; }
        public long category { get; set; }
        public DateTime submitted_date { get; set; }
    }
}
