﻿using System;

namespace Domain.Entities
{
    public class Recipe : BaseEntity
    {
        public string name { get; set; }
        public string ingredients { get; set; }
        public string instructions { get; set; }
        public long category { get; set; }
        public long user_id { get; set; }
        public DateTime submitted_date { get; set; }
    }
}
