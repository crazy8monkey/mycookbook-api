﻿using System;
namespace Domain.Entities
{
    public class RecipeImage: BaseEntity
    {
        public string name { get; set; }
        public byte[] picture { get; set; }
        public string picture_type { get; set; }
        public long recipe { get; set; }
        public Boolean is_primary { get; set; }
        public long size { get; set; }
    }
}
