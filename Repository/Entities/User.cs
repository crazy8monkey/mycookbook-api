﻿using System;
namespace Domain.Entities
{
    public class User : BaseEntity
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public DateTime registered_date { get; set; }
        public byte[] password { get; set; }
        public byte[] salt { get; set; }
        public string refresh_token { get; set; }
        public byte[] profile_pic { get; set; }
        public string profile_pic_type { get; set; }
        public Nullable<DateTime> reset_password_exp { get; set; }
        public Nullable<DateTime> refresh_token_exp { get; set; }
        public string user_role { get; set; }
    }
}