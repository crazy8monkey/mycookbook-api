﻿using System;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Domain
{
    public class MyCookBookDbContext : DbContext
    {
        public MyCookBookDbContext(DbContextOptions<MyCookBookDbContext> options) : base(options)
        {
        }

        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<RecipeImage> RecipeImages { get; set; }
        public DbSet<ErrorCategory> ErrorCategories { get; set; }
        public DbSet<Error> Errors { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("dbo");
            modelBuilder.Entity<Recipe>().ToTable("recipes");
            modelBuilder.Entity<Category>().ToTable("categories");
            modelBuilder.Entity<RecipeImage>().ToTable("recipe_images");
            modelBuilder.Entity<ErrorCategory>().ToTable("categories_error");
            modelBuilder.Entity<Error>().ToTable("errors");
            modelBuilder.Entity<User>().ToTable("users");

            base.OnModelCreating(modelBuilder);
        }
    }
}
