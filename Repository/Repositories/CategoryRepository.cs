﻿using System;
using System.Linq;
using System.Collections.Generic;
using Domain.Entities;
using System.Runtime.InteropServices;
using Domain.Abstraction;
using System.Linq.Expressions;

namespace Domain.Repositories
{
    internal sealed class CategoryRepository : ICategoryRepository
    {
        private readonly MyCookBookDbContext _dbContext;

        public CategoryRepository(MyCookBookDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Category Add(Category item)
        {
            _dbContext.Categories.Add(item);
            _dbContext.SaveChanges();
            return item;
        }

        public Category Get(Expression<Func<Category, bool>> whereExpression)
        {
            return _dbContext.Categories.Where(whereExpression).FirstOrDefault();
        }

        public List<Category> List(Expression<Func<Category, bool>> whereExpression)
        {
            if(whereExpression != null)
            {
                return _dbContext.Categories.Where(whereExpression).ToList();
            }

            return _dbContext.Categories.ToList();
        }

        public Category Update(Category item)
        {
            Category currentCategory = _dbContext.Categories.Where(category => category.id == item.id)
                                                            .Where(category => category.user_id == item.user_id).FirstOrDefault();
            currentCategory.name = item.name;
            _dbContext.SaveChanges();
            return item;
        }

        public void Remove(Category item)
        {
            _dbContext.Categories.Remove(item);
            _dbContext.SaveChanges();
        }

        public void RemoveByRange(List<Category> items)
        {
            _dbContext.Categories.RemoveRange(items);
            _dbContext.SaveChanges();
        }
    }
}