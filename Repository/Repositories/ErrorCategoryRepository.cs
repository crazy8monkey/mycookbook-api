﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Domain.Abstraction;
using Domain.Entities;

namespace Domain.Repositories
{
    public class ErrorCategoryRepository : IErrorCategoryRepository
    {
        private readonly MyCookBookDbContext _dbContext;

        public ErrorCategoryRepository(MyCookBookDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public ErrorCategory Add(ErrorCategory item)
        {
            _dbContext.ErrorCategories.Add(item);
            _dbContext.SaveChanges();
            return item;
        }

        public ErrorCategory Get(Expression<Func<ErrorCategory, bool>> whereExpression)
        {
            return _dbContext.ErrorCategories.Where(whereExpression).FirstOrDefault();
        }

        public List<ErrorCategory> List(Expression<Func<ErrorCategory, bool>> whereExpression)
        {
            if(whereExpression != null)
            {
                return _dbContext.ErrorCategories.Where(whereExpression).ToList();
            }
            return _dbContext.ErrorCategories.ToList();
        }

        public void Remove(ErrorCategory item)
        {
            _dbContext.ErrorCategories.Remove(item);
            _dbContext.SaveChanges();
        }

        public void RemoveByRange(List<ErrorCategory> items)
        {
            throw new NotImplementedException();
        }

        public ErrorCategory Update(ErrorCategory item)
        {
            ErrorCategory currentCategory = _dbContext.ErrorCategories.Where(category => category.id == item.id).FirstOrDefault();
            currentCategory.name = item.name;
            _dbContext.SaveChanges();
            return item;
        }
    }
}
