﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Domain.Abstraction;
using Domain.Entities;

namespace Domain.Repositories
{
    internal sealed class ErrorRepository : IErrorRepository
    {
        private readonly MyCookBookDbContext _dbContext;
        
        public ErrorRepository(MyCookBookDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Error Add(Error item)
        {
            _dbContext.Errors.Add(item);
            _dbContext.SaveChanges();
            return item;
        }

        public Error Get(Expression<Func<Error, bool>> whereExpression)
        {
            return _dbContext.Errors.Where(whereExpression).FirstOrDefault();
        }

        public List<Error> List(Expression<Func<Error, bool>> whereExpression)
        {
            if(whereExpression != null)
            {
                return _dbContext.Errors.Where(whereExpression).ToList();
            }

            return _dbContext.Errors.ToList();
        }

        public Error Update(Error item)
        {
            Error currentError = _dbContext.Errors.Where(error => error.id == item.id).FirstOrDefault();
            currentError.category = item.category;
            _dbContext.SaveChanges();
            return item;
        }

        public void Remove(Error item)
        {
            _dbContext.Errors.Remove(item);
            _dbContext.SaveChanges();
        }

        public void UpdateCategories(long current_category, long new_category)
        {
            List<Error> errors = _dbContext.Errors.Where(error => error.category == current_category).ToList();

            errors.ForEach(error => error.category = new_category);
            _dbContext.SaveChanges();
        }

        public void RemoveByRange(List<Error> items)
        {
            throw new NotImplementedException();
        }
    }
}
