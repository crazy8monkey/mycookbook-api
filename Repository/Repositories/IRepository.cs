﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Domain.Repositories
{
    public interface IRepository<T>
    {
        T Get(Expression<Func<T, bool>> whereExpression);
        T Add(T item);
        T Update(T item);
        List<T> List(Expression<Func<T, bool>> whereExpression);
        void Remove(T item);
        void RemoveByRange(List<T> items);
    }
}
