﻿using System;
using Domain.Abstraction;

namespace Domain.Repositories
{
    public interface IRepositoryManager
    {
        IRecipeRepository RecipeRepository { get; }
        IRecipeImageRepository RecipeImageRepository { get; }
        ICategoryRepository CategoryRepository { get; }
        IErrorCategoryRepository ErrorCategoryRepository { get; }
        IErrorRepository ErrorRepository { get; }
        IUserRepository UserRepository { get; }
    }
}
