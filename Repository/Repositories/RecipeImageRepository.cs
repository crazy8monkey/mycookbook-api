﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Domain.Abstraction;
using Domain.Entities;

namespace Domain.Repositories
{
    public class RecipeImageRepository : IRecipeImageRepository
    {
        private readonly MyCookBookDbContext _dbContext;

        public RecipeImageRepository(MyCookBookDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public RecipeImage Get(Expression<Func<RecipeImage, bool>> whereExpression)
        {
            throw new NotImplementedException();
        }

        public RecipeImage getMainImage(long recipe_id)
        {
            return _dbContext.RecipeImages.Where(image => image.is_primary == true)
                                          .Where(image => image.recipe == recipe_id).FirstOrDefault();
        }

        public List<RecipeImage> List(Expression<Func<RecipeImage, bool>> whereExpression)
        {
            if(whereExpression != null)
            {
                return _dbContext.RecipeImages.Where(whereExpression).ToList();
            }
            return _dbContext.RecipeImages.ToList();
        }

        public List<RecipeImage> Add(List<RecipeImage> images)
        {
            _dbContext.RecipeImages.AddRange(images);
            _dbContext.SaveChanges();
            return images;
        }

        public List<RecipeImage> Update(long recipe_id, List<RecipeImage> add)
        {
            List<RecipeImage> remove = _dbContext.RecipeImages.Where(image => image.recipe == recipe_id).ToList();
            _dbContext.RecipeImages.RemoveRange(remove);
            _dbContext.RecipeImages.AddRange(add);
            _dbContext.SaveChanges();
            return add;
        }

        public void RemoveByRecipe(long recipe_id)
        {
            List<RecipeImage> images = _dbContext.RecipeImages.Where(image => image.recipe == recipe_id).ToList();
            _dbContext.RemoveRange(images);
            _dbContext.SaveChanges();
        }

        public RecipeImage Update(RecipeImage item)
        {
            throw new NotImplementedException();
        }

        //unimplemented methods
        public RecipeImage Add(RecipeImage item)
        {
            throw new NotImplementedException();
        }

        public void Remove(RecipeImage item)
        {
            throw new NotImplementedException();
        }

        public void RemoveByRange(List<RecipeImage> items)
        {
            throw new NotImplementedException();
        }
    }
}
