﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Domain.Entities;
using Libraries;

namespace Domain.Repositories
{
    internal sealed class RecipeRepository : IRecipeRepository
    {
        private readonly MyCookBookDbContext _dbContext;

        public RecipeRepository(MyCookBookDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Recipe Add(Recipe item)
        {
            _dbContext.Recipes.Add(item);
            _dbContext.SaveChanges();
            return item;
        }

        public Recipe Get(Expression<Func<Recipe, bool>> whereExpression)
        {
            return _dbContext.Recipes.Where(whereExpression).FirstOrDefault();
        }

        public List<Recipe> List(Expression<Func<Recipe, bool>> whereExpression)
        {
            if(whereExpression != null)
            {
                return _dbContext.Recipes.Where(whereExpression).ToList();
            }
            return _dbContext.Recipes.ToList();
        }

        public void Remove(Recipe item)
        {
            Recipe recipe = _dbContext.Recipes.Where(recipe => recipe.id == item.id)
                                              .Where(recipe => recipe.user_id == item.user_id).FirstOrDefault();
            _dbContext.Recipes.Remove(recipe);
            _dbContext.SaveChanges();
        }

        public void RemoveByRange(List<Recipe> items)
        {
            _dbContext.Recipes.RemoveRange(items);
            _dbContext.SaveChanges();
        }

        public Recipe Update(Recipe item)
        {
            Recipe currentRecipe = _dbContext.Recipes.Where(recipe => recipe.id == item.id).FirstOrDefault();
            currentRecipe.name = item.name;
            currentRecipe.ingredients = item.ingredients;
            currentRecipe.instructions = item.instructions;
            currentRecipe.category = item.category;
            currentRecipe.user_id = item.user_id;
            _dbContext.SaveChanges();
            return item;
        }
        
        public void UpdateCategories(Recipe recipe)
        {
            List<Recipe> recipes = _dbContext.Recipes.Where(recipe => recipe.category == recipe.category)
                                            .Where(recipe => recipe.user_id == recipe.user_id).ToList();

            recipes.ForEach(recipe => recipe.category = 0);
            _dbContext.SaveChanges();
        }
    }
}
