﻿using System;
using Domain.Abstraction;

namespace Domain.Repositories
{
    public class RepositoryManager : IRepositoryManager
    {
        private readonly Lazy<IRecipeRepository> _lazyRecipeRepository;
        private readonly Lazy<ICategoryRepository> _lazyCategoryRepository;
        private readonly Lazy<IRecipeImageRepository> _lazyRecipeImageRepository;
        private readonly Lazy<IErrorCategoryRepository> _lazyErrorCategoryRepository;
        private readonly Lazy<IErrorRepository> _lazyErrorRepository;
        private readonly Lazy<IUserRepository> _lazyUserRepository;

        public RepositoryManager(MyCookBookDbContext dbContext)
        {
            _lazyRecipeRepository = new Lazy<IRecipeRepository>(() => new RecipeRepository(dbContext));
            _lazyCategoryRepository = new Lazy<ICategoryRepository>(() => new CategoryRepository(dbContext));
            _lazyRecipeImageRepository = new Lazy<IRecipeImageRepository>(() => new RecipeImageRepository(dbContext));
            _lazyErrorCategoryRepository = new Lazy<IErrorCategoryRepository>(() => new ErrorCategoryRepository(dbContext));
            _lazyErrorRepository = new Lazy<IErrorRepository>(() => new ErrorRepository(dbContext));
            _lazyUserRepository = new Lazy<IUserRepository>(() => new UserRepository(dbContext));
        }

        public IRecipeRepository RecipeRepository => _lazyRecipeRepository.Value;
        public ICategoryRepository CategoryRepository => _lazyCategoryRepository.Value;
        public IRecipeImageRepository RecipeImageRepository => _lazyRecipeImageRepository.Value;
        public IErrorCategoryRepository ErrorCategoryRepository => _lazyErrorCategoryRepository.Value;
        public IErrorRepository ErrorRepository => _lazyErrorRepository.Value;
        public IUserRepository UserRepository => _lazyUserRepository.Value;
    }
}
