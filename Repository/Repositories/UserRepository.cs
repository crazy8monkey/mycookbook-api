﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Domain.Abstraction;
using Domain.Entities;

namespace Domain.Repositories
{
    internal sealed class UserRepository : IUserRepository
    {
        private readonly MyCookBookDbContext _dbContext;

        public UserRepository(MyCookBookDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<User> List(Expression<Func<User, bool>> whereExpression)
        {
            if(whereExpression != null)
            {
                return _dbContext.Users.Where(whereExpression).ToList();
            }
            return _dbContext.Users.ToList();
        }

        public User Add(User item)
        {
            _dbContext.Users.Add(item);
            _dbContext.SaveChanges();
            return item;
        }

        public User Update(User item)
        {
            User currentUser = _dbContext.Users.Where(user => user.id == item.id).FirstOrDefault();
            currentUser.first_name = item.first_name;
            currentUser.last_name = item.last_name;
            currentUser.password = item.password;
            currentUser.salt = item.salt;
            currentUser.refresh_token = item.refresh_token;
            currentUser.profile_pic = item.profile_pic;
            currentUser.profile_pic_type = item.profile_pic_type;
            currentUser.reset_password_exp = item.reset_password_exp;
            currentUser.refresh_token_exp = item.refresh_token_exp;
            _dbContext.SaveChanges();
            return item;
        }

        public User Get(Expression<Func<User, bool>> whereExpression)
        {
            return _dbContext.Users.Where(whereExpression).FirstOrDefault();
        }

        public void Remove(User item)
        {
            _dbContext.Users.Remove(item);
            _dbContext.SaveChanges();
            throw new NotImplementedException();
        }

        public void RemoveByRange(List<User> items)
        {
            throw new NotImplementedException();
        }
    }
}
