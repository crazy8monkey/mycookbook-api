﻿using System;
using Contracts;

namespace Service.Abstractions
{
    public interface ICategoryService : IService<CategoryDTO>
    {
        Boolean GetByName(string name);
        Boolean GetByName(string id, string name);
    }
}
