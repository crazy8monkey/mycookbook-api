﻿using System;
using Contracts;

namespace Service.Abstractions
{
    public interface IErrorCategoryService : IService<ErrorCategoryDTO>
    {
        Boolean getByName(string name);
    }
}
