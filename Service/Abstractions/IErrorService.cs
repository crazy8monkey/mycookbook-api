﻿using System;
using Contracts;

namespace Service.Abstractions
{
    public interface IErrorService : IService<ErrorDTO>
    {
        long getUser();
    }
}
