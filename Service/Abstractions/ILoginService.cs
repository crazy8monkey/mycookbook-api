﻿using System;
using Contracts;

namespace Service.Abstractions
{
    public interface ILoginService : IService<UserDTO>
    {
        Boolean ValidateCredentials(LoginDTO user);
        UserDTO LoginUser(LoginDTO login);
    }
}
