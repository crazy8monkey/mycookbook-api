﻿using System;
using Contracts;

namespace Service.Abstractions
{
    public interface IPasswordService : IService<UserDTO>
    {
        void UpdatePassword(NewPasswordDTO passwords);
        UserDTO GetByEmail(string email, string role);
        DateTime ResetPassword(string id);
    }
}
