﻿using System;
using Contracts;

namespace Service.Abstractions
{
    public interface IProfileService : IService<UserDTO>
    {
        object GetSessionUser();
        void Update(ProfileDTO profile);
        ImageDTO NewImage(ProfileImageDTO image);
    }
}
