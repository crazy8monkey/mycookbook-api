﻿using System;
using System.Collections.Generic;
using Contracts;
using Libraries;

namespace Service.Abstractions
{
    public interface IRecipeImageService : IService<RecipeImageDTO>
    {
        List<RecipeImageDTO> List(string recipe_id);
        List<RecipeImageDTO> Add(RecipeImageUploadDTO images);
        List<RecipeImageDTO> Update(RecipeImageUploadDTO images);
    }
}
