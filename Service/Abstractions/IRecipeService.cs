﻿using System;
using System.Collections.Generic;
using Contracts;

namespace Service.Abstractions
{
    public interface IRecipeService : IService<RecipeDTO>
    {
    }
}
