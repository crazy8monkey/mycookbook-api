﻿using System;
using Contracts;

namespace Service.Abstractions
{
    public interface ISignupService : IService<RecipeDTO>
    {
        Boolean EmailRegistered(string email, string role);
    }
}
