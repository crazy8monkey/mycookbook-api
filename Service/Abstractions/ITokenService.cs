﻿using System;
using Contracts;

namespace Service.Abstractions
{
    public interface ITokenService : IService<UserDTO>
    {
        object NewToken(RefreshTokenDTO refreshToken);
        Boolean ValidToken(RefreshTokenDTO refreshToken);
        string GenerateToken(string id, string role);
    }
}
