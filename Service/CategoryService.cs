﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Contracts;
using Domain.Entities;
using Domain.Repositories;
using Libraries;
using Microsoft.AspNetCore.Http;
using Service.Abstractions;

namespace Service
{
    public class CategoryService : ICategoryService
    {
        private readonly IRepositoryManager _repositoryManager;
        private CurrentSessionDTO _currentSession;

        public CategoryService(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
            _currentSession = new CurrentSessionDTO();
        }

        public void setRequest(HttpRequest request)
        {
            _currentSession = request.HttpContext.Features.Get<CurrentSessionDTO>();
        }

        public CategoryDTO Add(CategoryDTO item)
        {
            Category newCategory = _repositoryManager.CategoryRepository.Add(new Category
            {
                name = item.name,
                user_id = _currentSession.user
            });
            item.id = CryptoEngine.Encrypt(newCategory.id);
            return item;
        }

        public CategoryDTO Get(string id)
        {
            var categoryId = CryptoEngine.Decrypt(id);
            Expression<Func<Category, bool>> getCategoryExpression = category => category.id == categoryId;
            Category currentCategory = _repositoryManager.CategoryRepository.Get(getCategoryExpression);
            return new CategoryDTO
            {
                id = CryptoEngine.Encrypt(currentCategory.id),
                name = currentCategory.name
            };
        }

        public List<CategoryDTO> List()
        {
            Expression<Func<Category, bool>> categoriesByUser = category => category.user_id == _currentSession.user;
            var currentCategories = _repositoryManager.CategoryRepository.List(categoriesByUser);
            List<CategoryDTO> categories = new List<CategoryDTO>();
            categories.Add(new CategoryDTO
            {
                id = "-1",
                name = "All Recipes"
            });
            categories.Add(new CategoryDTO
            {
                id = "0",
                name = "Uncategorized"
            });

            List<CategoryDTO> categorieViewModels = currentCategories.Select(category => new CategoryDTO
            {
                id = CryptoEngine.Encrypt(category.id),
                name = category.name,
            }).OrderBy(c => c.name).ToList();
            categories.AddRange(categorieViewModels);

            return categories;
        }

        public void Remove(string id)
        {
            //getting category
            var categoryId = CryptoEngine.Decrypt(id);
            Expression<Func<Category, bool>> getCatgory = category => category.id == categoryId && category.user_id == _currentSession.user;
            Category currentCategory = _repositoryManager.CategoryRepository.Get(getCatgory);

            //getting recipe
            Expression<Func<Recipe, bool>> getRecipe = recipe => recipe.category == categoryId;
            Recipe currentRecipe = _repositoryManager.RecipeRepository.Get(getRecipe);

            //update current recipes to uncategorized before removing
            _repositoryManager.RecipeRepository.UpdateCategories(currentRecipe);

            //remove category
            _repositoryManager.CategoryRepository.Remove(currentCategory);            
        }

        public CategoryDTO Update(CategoryDTO item)
        {
            _repositoryManager.CategoryRepository.Update(new Category
            {
                id = CryptoEngine.Decrypt(item.id),
                name = item.name
            });
            return item;
        }

        public Boolean GetByName(string name)
        {
            Expression<Func<Category, bool>> getByName = category => category.name == name;

            var isCategoryPresent = _repositoryManager.CategoryRepository.Get(getByName);
            if (isCategoryPresent != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean GetByName(string id, string name)
        {
            var decryptId = CryptoEngine.Decrypt(id);
            Expression<Func<Category, bool>> getByName = category => category.name == name && category.id == decryptId;
            
            var isCategoryPresent = _repositoryManager.CategoryRepository.Get(getByName);
            if(isCategoryPresent != null)
            {
                return true;
            } else
            {
                return false;
            }
        }
    }
}
