﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Contracts;
using Domain.Entities;
using Domain.Repositories;
using Libraries;
using Microsoft.AspNetCore.Http;
using Service.Abstractions;

namespace Service
{
    public class ErrorCategoryService : IErrorCategoryService
    {
        private readonly IRepositoryManager _repositoryManager;

        public ErrorCategoryService(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
        }

        public ErrorCategoryDTO Add(ErrorCategoryDTO item)
        {
            ErrorCategory newCategory = _repositoryManager.ErrorCategoryRepository.Add(new ErrorCategory
            {
                name = item.name
            });
            item.id = CryptoEngine.Encrypt(newCategory.id);
            return item;
        }

        public ErrorCategoryDTO Get(string id)
        {
            var categoryId = CryptoEngine.Decrypt(id);
            Expression<Func<ErrorCategory, bool>> getCategoryExpression = category => category.id == categoryId;
            ErrorCategory currentCategory = _repositoryManager.ErrorCategoryRepository.Get(getCategoryExpression);
            return new ErrorCategoryDTO
            {
                id = CryptoEngine.Encrypt(currentCategory.id),
                name = currentCategory.name
            };
        }

        public ErrorCategoryDTO Update(ErrorCategoryDTO item)
        {
            _repositoryManager.ErrorCategoryRepository.Update(new ErrorCategory
            {
                id = CryptoEngine.Decrypt(item.id),
                name = item.name
            });
            return item;
        }

        public List<ErrorCategoryDTO> List()
        {
            return _repositoryManager.ErrorCategoryRepository.List(null).Select(category => new ErrorCategoryDTO
            {
                id = CryptoEngine.Encrypt(category.id),
                name = category.name,
            }).ToList();
        }

        public void Remove(string id)
        {
            var categoryId = CryptoEngine.Decrypt(id);
            Expression<Func<ErrorCategory, bool>> getCategory = category => category.id == categoryId;
            ErrorCategory currentCategory = _repositoryManager.ErrorCategoryRepository.Get(getCategory);

            //updating error category
            _repositoryManager.ErrorRepository.UpdateCategories(categoryId, 0);

            //removing entity
            _repositoryManager.ErrorCategoryRepository.Remove(currentCategory);
            
        }

        public void setRequest(HttpRequest request)
        {
            throw new NotImplementedException();
        }

        

        public bool getByName(string name)
        {
            Expression<Func<ErrorCategory, bool>> getCategoryExpression = category => category.name == name;
            ErrorCategory currentCategory = _repositoryManager.ErrorCategoryRepository.Get(getCategoryExpression);
            if(currentCategory != null)
            {
                return true;
            }

            return false;
        }
    }
}
