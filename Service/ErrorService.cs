﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Contracts;
using Domain.Entities;
using Domain.Repositories;
using Libraries;
using Microsoft.AspNetCore.Http;
using Service.Abstractions;

namespace Service
{
    public class ErrorService : IErrorService
    {
        private readonly IRepositoryManager _repositoryManager;
        private CurrentSessionDTO _currentSession; 

        public ErrorService(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
            _currentSession = new CurrentSessionDTO();
        }

        public void setRequest(HttpRequest request)
        {
            _currentSession = request.HttpContext.Features.Get<CurrentSessionDTO>();
        }

        public long getUser()
        {
            return _currentSession.user;
        }

        public ErrorDTO Add(ErrorDTO item)
        {
            Error newErrorData = new Error();
            newErrorData.user_id = _currentSession.user;
            newErrorData.description = item.description;
            newErrorData.submitted_date = DateTime.UtcNow;
            if(item.category != "0")
            {
                newErrorData.category = CryptoEngine.Decrypt(item.category);
            } else
            {
                newErrorData.category = Convert.ToInt64(item.category);
            }

            if (_currentSession.user != 0)
            {
                Expression<Func<User, bool>> getUserExpression = user => user.id == _currentSession.user;
                User currentUser = _repositoryManager.UserRepository.Get(getUserExpression);
                newErrorData.first_name = currentUser.first_name;
                newErrorData.last_name = currentUser.last_name;
            } else
            {
                newErrorData.first_name = item.first_name;
                newErrorData.last_name = item.last_name;
            }

            Error newError = _repositoryManager.ErrorRepository.Add(newErrorData);
            item.id = CryptoEngine.Encrypt(newError.id);
            return item;
        }

        public ErrorDTO Get(string id)
        {
            var errorId = CryptoEngine.Decrypt(id);
            Expression<Func<Error, bool>> getErrorExpression = error => error.id == errorId;
            Error currentError = _repositoryManager.ErrorRepository.Get(getErrorExpression);
            return new ErrorDTO
            {
                id = id,
                first_name = currentError.first_name,
                last_name = currentError.last_name,
                user_id = currentError.user_id,
                submitted_date = String.Format("{0} {1}", currentError.submitted_date.ToShortDateString(), currentError.submitted_date.ToShortTimeString()),
                description = currentError.description,
                category = CryptoEngine.Encrypt(currentError.category)
            };
        }

        public List<ErrorDTO> List()
        {
            List<Error> currentErrors = _repositoryManager.ErrorRepository.List(null);

            return currentErrors.Select(error =>
            {
                
                ErrorDTO currentErrorDto = new ErrorDTO();
                currentErrorDto.id = CryptoEngine.Encrypt(error.id);
                currentErrorDto.user_id = error.user_id;
                if (error.user_id != 0)
                {
                    Expression<Func<User, bool>> getUserExpression = user => user.id == (long)error.user_id;
                    User getUser = _repositoryManager.UserRepository.Get(getUserExpression);
                    currentErrorDto.first_name = getUser.first_name;
                    currentErrorDto.last_name = getUser.last_name;
                } else
                {
                    currentErrorDto.first_name = error.first_name;
                    currentErrorDto.last_name = error.last_name;
                }
                if(error.category != 0)
                {
                    Expression<Func<ErrorCategory, bool>> getCategoryExpression = category => category.id == error.category;
                    ErrorCategory currentCategory = _repositoryManager.ErrorCategoryRepository.Get(getCategoryExpression);
                    currentErrorDto.category = currentCategory.name;
                }

                currentErrorDto.submitted_date = String.Format("{0} {1}", error.submitted_date.ToShortDateString(), error.submitted_date.ToShortTimeString());

                return currentErrorDto;
            }).ToList();
        }
    
        public ErrorDTO Update(ErrorDTO item)
        {
            _repositoryManager.ErrorRepository.Update(new Error
            {
                id = CryptoEngine.Decrypt(item.id),
                category = CryptoEngine.Decrypt(item.category)
            });

            return item;
        }

        public void Remove(string id)
        {
            var errorId = CryptoEngine.Decrypt(id);
            Expression<Func<Error, bool>> getError = error => error.id == errorId;
            Error currentError = _repositoryManager.ErrorRepository.Get(getError);
            _repositoryManager.ErrorRepository.Remove(currentError); 
        }
    }
}
