﻿using System;
using System.Collections.Generic;
using Libraries;
using Microsoft.AspNetCore.Http;

namespace Service
{
    public interface IService<T>
    {
        void setRequest(HttpRequest request);
        T Get(string id);
        T Add(T item);
        T Update(T item);
        List<T> List();
        void Remove(string id);
    }
}
