﻿using System;
using Service.Abstractions;

namespace Service
{
    public interface IServiceManager
    {
        IRecipeService RecipeService { get; }
        ICategoryService CategoryService { get; }
        IUserService UserService { get; }
        IProfileService ProfileService { get; }
        ITokenService TokenService { get; }
        ILoginService LoginService { get; }
        IPasswordService PasswordService { get; }
        ISignupService SignupService { get; }
        IErrorCategoryService ErrorCategoryService { get; }
        IErrorService ErrorService { get; }
        IRecipeImageService RecipeImageService { get; }
    }
}
