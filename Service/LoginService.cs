﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Contracts;
using Domain.Entities;
using Domain.Repositories;
using Libraries;
using Microsoft.AspNetCore.Http;
using Service.Abstractions;

namespace Service
{
    public class LoginService : ILoginService
    {
        private readonly IRepositoryManager _repositoryManager;
        private JWTTokenHandler jwtToken = new JWTTokenHandler();

        public LoginService(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
        }

        public bool ValidateCredentials(LoginDTO user)
        {
            Expression<Func<User, bool>> getUserEmailExpression = u => u.email == user.email && u.user_role == user.role;
            User currentUserCheck = _repositoryManager.UserRepository.Get(getUserEmailExpression);

            if (currentUserCheck != null)
            {
                byte[] userPassword = CryptoEngine.ComputeHash(user.password, currentUserCheck.salt);
                if (userPassword.Length == currentUserCheck.password.Length)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public UserDTO LoginUser(LoginDTO login)
        {
            Expression<Func<User, bool>> getUserEmailExpression = u => u.email == login.email && u.user_role == login.role;
            User currentUser = _repositoryManager.UserRepository.Get(getUserEmailExpression);

            currentUser.refresh_token = jwtToken.RefreshToken();
            currentUser.refresh_token_exp = DateTime.Now.AddHours(7);
            _repositoryManager.UserRepository.Update(currentUser);

            UserDTO getUser = new UserDTO();
            getUser.id = CryptoEngine.Encrypt(currentUser.id);
            getUser.refresh_token = currentUser.refresh_token;
            getUser.first_name = currentUser.first_name;
            getUser.last_name = currentUser.last_name;
            getUser.email = login.email;
            getUser.profile_pic = currentUser.profile_pic;
            getUser.profile_pic_type = currentUser.profile_pic_type;
            getUser.user_role = currentUser.user_role;
            return getUser;
        }

        /* unused methods */
        public UserDTO Add(UserDTO item)
        {
            throw new NotImplementedException();
        }

        public UserDTO Get(string id)
        {
            throw new NotImplementedException();
        }

        public List<UserDTO> List()
        {
            throw new NotImplementedException();
        }

        public void Remove(string id)
        {
            throw new NotImplementedException();
        }

        public void setRequest(HttpRequest request)
        {
            throw new NotImplementedException();
        }

        public UserDTO Update(UserDTO item)
        {
            throw new NotImplementedException();
        }

        
    }
}
