﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Contracts;
using Domain.Entities;
using Domain.Repositories;
using Libraries;
using Microsoft.AspNetCore.Http;
using Service.Abstractions;

namespace Service
{
    public class PasswordService : IPasswordService
    {
        private readonly IRepositoryManager _repositoryManager;
        private JWTTokenHandler jwtToken = new JWTTokenHandler();

        public PasswordService(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
        }

        public void UpdatePassword(NewPasswordDTO passwords)
        {
            var userId = CryptoEngine.Decrypt(passwords.user_id);
            Expression<Func<User, bool>> getUserByIdExpression = u => u.id == userId;
            User currentUser = _repositoryManager.UserRepository.Get(getUserByIdExpression);

            byte[] newSalt = CryptoEngine.GenerateSalt();
            byte[] newPassword = CryptoEngine.ComputeHash(passwords.retype_password, newSalt);

            currentUser.password = newPassword;
            currentUser.salt = newSalt;

            _repositoryManager.UserRepository.Update(currentUser);
        }

        public UserDTO GetByEmail(string email, string role)
        {
            Expression<Func<User, bool>> getUserEmailExpression = u => u.email == email && u.user_role == role;
            User currentUser = _repositoryManager.UserRepository.Get(getUserEmailExpression);

            if (currentUser != null)
            {
                currentUser.refresh_token = jwtToken.RefreshToken();
                currentUser.refresh_token_exp = DateTime.Now.AddHours(7);
                _repositoryManager.UserRepository.Update(currentUser);

                UserDTO getUser = new UserDTO();
                getUser.id = CryptoEngine.Encrypt(currentUser.id);
                getUser.refresh_token = currentUser.refresh_token;
                getUser.first_name = currentUser.first_name;
                getUser.last_name = currentUser.last_name;
                getUser.email = email;
                getUser.profile_pic = currentUser.profile_pic;
                getUser.profile_pic_type = currentUser.profile_pic_type;
                getUser.user_role = currentUser.user_role;
                return getUser;
            }
            else
            {
                return null;
            }
        }

        public DateTime ResetPassword(string id)
        {
            var userId = CryptoEngine.Decrypt(id);
            Expression<Func<User, bool>> getUserExpression = user => user.id == userId;
            User currentUser = _repositoryManager.UserRepository.Get(getUserExpression);
            currentUser.reset_password_exp = DateTime.Now.AddHours(1);
            _repositoryManager.UserRepository.Update(currentUser);
            return (DateTime)currentUser.reset_password_exp;
        }

        /* unused methods */
        public UserDTO Add(UserDTO item)
        {
            throw new NotImplementedException();
        }

        public UserDTO Get(string id)
        {
            throw new NotImplementedException();
        }

        public List<UserDTO> List()
        {
            throw new NotImplementedException();
        }

        public void Remove(string id)
        {
            throw new NotImplementedException();
        }

        public void setRequest(HttpRequest request)
        {
            throw new NotImplementedException();
        }

        public UserDTO Update(UserDTO item)
        {
            throw new NotImplementedException();
        }
    }
}
