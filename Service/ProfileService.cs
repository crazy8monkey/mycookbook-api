﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using Contracts;
using Domain.Entities;
using Domain.Repositories;
using Libraries;
using Microsoft.AspNetCore.Http;
using Service.Abstractions;

namespace Service
{
    public class ProfileService : IProfileService
    {
        private readonly IRepositoryManager _repositoryManager;
        private CurrentSessionDTO _currentSession;
        private HttpRequest _request;

        public ProfileService(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
            _currentSession = new CurrentSessionDTO();
        }

        public void setRequest(HttpRequest request)
        {
            _request = request;
            _currentSession = request.HttpContext.Features.Get<CurrentSessionDTO>();
        }

        public object GetSessionUser()
        {
            Expression<Func<User, bool>> getUserExpression = user => user.id == _currentSession.user && user.user_role == _currentSession.role;
            User currentUser = _repositoryManager.UserRepository.Get(getUserExpression);

            dynamic SessionUser = new System.Dynamic.ExpandoObject();
            SessionUser.first_name = currentUser.first_name;
            SessionUser.last_name = currentUser.last_name;
            SessionUser.email = currentUser.email;
            SessionUser.profile = currentUser.profile_pic;
            SessionUser.profile_type = currentUser.profile_pic_type;
            return SessionUser;
        }

        public void Update(ProfileDTO profile)
        {
            Expression<Func<User, bool>> getUserExpression = user => user.id == _currentSession.user && user.user_role == _currentSession.role;
            User currentUser = _repositoryManager.UserRepository.Get(getUserExpression);

            currentUser.first_name = profile.first_name;
            currentUser.last_name = profile.last_name;

            if (profile.new_password != "" && profile.new_password != null)
            {

                byte[] newSalt = CryptoEngine.GenerateSalt();
                byte[] newPasswordHash = CryptoEngine.ComputeHash(profile.new_password, newSalt);

                currentUser.password = newPasswordHash;
                currentUser.salt = newSalt;

                _repositoryManager.UserRepository.Update(currentUser);
            }
        }

        public ImageDTO NewImage(ProfileImageDTO image)
        {
            Expression<Func<User, bool>> getUserExpression = user => user.id == _currentSession.user && user.user_role == _currentSession.role;
            User currentUser = _repositoryManager.UserRepository.Get(getUserExpression);

            using (Stream stream = new FileStream(image.imageName, FileMode.Create))
            {
                image.imageFile.CopyTo(stream);
                byte[] picture = File.ReadAllBytes(image.imageName);
                string type = String.Format("image/{0}", Path.GetExtension(image.imageName).Substring(1));

                currentUser.profile_pic = picture;
                currentUser.profile_pic_type = type;

                _repositoryManager.UserRepository.Update(currentUser);
                return new ImageDTO
                {
                    picture = picture,
                    type = type
                };
            }
        }

        /* unused methods */
        public UserDTO Add(UserDTO item)
        {
            throw new NotImplementedException();
        }

        public UserDTO Get(string id)
        {
            throw new NotImplementedException();
        }

        public List<UserDTO> List()
        {
            throw new NotImplementedException();
        }

        public void Remove(string id)
        {
            throw new NotImplementedException();
        }

        public UserDTO Update(UserDTO item)
        {
            throw new NotImplementedException();
        }
    }
}
