﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using Contracts;
using Domain.Entities;
using Domain.Repositories;
using Libraries;
using Microsoft.AspNetCore.Http;
using Service.Abstractions;

namespace Service
{
    public class RecipeImageService : IRecipeImageService
    {
        private readonly IRepositoryManager _repositoryManager;
        
        public RecipeImageService(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
        }

        public List<RecipeImageDTO> Add(RecipeImageUploadDTO images)
        {
            

            List<RecipeImage> newImages = new List<RecipeImage>();
            foreach (IFormFile image in images.imageFiles)
            {
                using (Stream stream = new FileStream(image.FileName, FileMode.Create))
                {
                    image.CopyTo(stream);
                    RecipeImage newImage = new RecipeImage();
                    newImage.name = image.FileName;
                    newImage.picture = File.ReadAllBytes(image.FileName);
                    newImage.picture_type = image.ContentType;
                    newImage.recipe = CryptoEngine.Decrypt(images.recipe);
                    newImage.is_primary = true;
                    newImage.size = image.Length;
                    newImages.Add(newImage);
                    File.Delete(image.FileName);
                }
            }

            List<RecipeImage> newRecipeImagesSaved = _repositoryManager.RecipeImageRepository.Add(newImages);

            return newRecipeImagesSaved.Select(image => new RecipeImageDTO
            {
                id = CryptoEngine.Encrypt(image.id),
                name = image.name,
                picture = image.picture,
                picture_type = image.picture_type,
                recipe = CryptoEngine.Encrypt(image.recipe),
                is_primary = image.is_primary,
                size = image.size
            }).ToList();
        }

        public List<RecipeImageDTO> Update(RecipeImageUploadDTO images)
        {
            long recipeId = CryptoEngine.Decrypt(images.recipe);
            Expression<Func<RecipeImage, bool>> imagesByRecipe = image => image.recipe == recipeId;
            List<RecipeImage> currentRecipeImages = _repositoryManager.RecipeImageRepository.List(imagesByRecipe);
            List<RecipeImage> addingImages = new List<RecipeImage>();

            foreach (IFormFile image in images.imageFiles)
            {
                using (Stream stream = new FileStream(image.FileName, FileMode.Create))
                {
                    image.CopyTo(stream);
                    RecipeImage newImage = new RecipeImage();
                    newImage.name = image.FileName;
                    newImage.picture = File.ReadAllBytes(image.FileName);
                    newImage.picture_type = image.ContentType;
                    newImage.recipe = CryptoEngine.Decrypt(images.recipe);
                    newImage.is_primary = true;
                    newImage.size = image.Length;
                    addingImages.Add(newImage);
                    File.Delete(image.FileName);
                }
            }

            List<RecipeImage> updateRecipeImages = _repositoryManager.RecipeImageRepository.Update(recipeId, addingImages);
            return updateRecipeImages.Select(image => new RecipeImageDTO
            {
                id = CryptoEngine.Encrypt(image.id),
                name = image.name,
                picture = image.picture,
                picture_type = image.picture_type,
                recipe = CryptoEngine.Encrypt(image.recipe),
                is_primary = image.is_primary,
                size = image.size
            }).ToList();
        }

        public List<RecipeImageDTO> List(string recipe_id)
        {
            var recipeId = CryptoEngine.Decrypt(recipe_id);
            Expression<Func<RecipeImage, bool>> imagesByRecipe = image => image.recipe == recipeId;
            List<RecipeImage> currentRecipeImages = _repositoryManager.RecipeImageRepository.List(imagesByRecipe);

            return currentRecipeImages.Select(image => new RecipeImageDTO
            {
                id = CryptoEngine.Encrypt(image.id),
                is_primary = image.is_primary,
                name = image.name,
                picture = image.picture,
                picture_type = image.picture_type,
                recipe = CryptoEngine.Encrypt(image.recipe)
            }).ToList();
        }

        public void Remove(string id)
        {
            throw new NotImplementedException();
        }

        public void setRequest(HttpRequest request)
        {
            throw new NotImplementedException();
        }

        public RecipeImageDTO Update(RecipeImageDTO item)
        {
            throw new NotImplementedException();
        }

        //umimplemented methods
        public RecipeImageDTO Add(RecipeImageDTO item)
        {
            throw new NotImplementedException();
        }

        public List<RecipeImageDTO> List()
        {
            throw new NotImplementedException();
        }

        public RecipeImageDTO Get(string id)
        {
            throw new NotImplementedException();
        }
    }
}
