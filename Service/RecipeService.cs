﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Contracts;
using Domain.Entities;
using Domain.Repositories;
using Libraries;
using Microsoft.AspNetCore.Http;
using Service.Abstractions;

namespace Service
{
    public class RecipeService : IRecipeService
    {
        private readonly IRepositoryManager _repositoryManager;
        private CurrentSessionDTO _currentSession;

        public RecipeService(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
            _currentSession = new CurrentSessionDTO();
        }

        public void setRequest(HttpRequest request)
        {
            _currentSession = request.HttpContext.Features.Get<CurrentSessionDTO>();
        }

        public RecipeDTO Add(RecipeDTO item)
        {
            long category = 0;
            if (item.category != "0")
            {
                category = CryptoEngine.Decrypt(item.category);
            }

            Recipe newRecipe = _repositoryManager.RecipeRepository.Add(new Recipe
            {
                name = item.name,
                ingredients = item.ingredients,
                instructions = item.instructions,
                category = category,
                user_id = _currentSession.user,
                submitted_date = DateTime.UtcNow,
            });
            item.id = CryptoEngine.Encrypt(newRecipe.id);
            return item;
        }

        public RecipeDTO Get(string id)
        {
            var recipeId = CryptoEngine.Decrypt(id);
            Expression<Func<Recipe, bool>> getRecipeExpression = recipe => recipe.id == recipeId && recipe.user_id == _currentSession.user;

            Recipe currentRecipe = _repositoryManager.RecipeRepository.Get(getRecipeExpression);
            return new RecipeDTO
            {
                id = id,
                name = currentRecipe.name,
                ingredients = currentRecipe.ingredients,
                instructions = currentRecipe.instructions,
                category = CryptoEngine.Encrypt(currentRecipe.category)
            };
        }

        public List<RecipeDTO> List()
        {
            Expression<Func<Recipe, bool>> recipesByUser = recipe => recipe.user_id == _currentSession.user;
            List<Recipe> totalRecipes = _repositoryManager.RecipeRepository.List(recipesByUser);
            List<RecipeDTO> recipes = new List<RecipeDTO>();
            foreach (Recipe currentRecipe in totalRecipes)
            {
                
                RecipeDTO recipe = new RecipeDTO();
                recipe.id = CryptoEngine.Encrypt(currentRecipe.id);
                recipe.name = currentRecipe.name;
                recipe.ingredients = currentRecipe.ingredients;
                recipe.instructions = currentRecipe.instructions;

                if (currentRecipe.category != 0)
                {
                    recipe.category = CryptoEngine.Encrypt(currentRecipe.category);
                }
                else
                {
                    recipe.category = "0";
                }

                RecipeImage mainImage = _repositoryManager.RecipeImageRepository.getMainImage(currentRecipe.id);

                if (mainImage != null)
                {
                    ImageDTO recipePrimaryImage = new ImageDTO();
                    recipePrimaryImage.picture = mainImage.picture;
                    recipePrimaryImage.type = mainImage.picture_type;
                    recipe.image = recipePrimaryImage;
                }

                recipes.Add(recipe);
            }

            return recipes;
        }

        public RecipeDTO Update(RecipeDTO item)
        {
            long category = 0;
            if (item.category != "0")
            {
                category = CryptoEngine.Decrypt(item.category);
            }

            _repositoryManager.RecipeRepository.Update(new Recipe
            {
                id = CryptoEngine.Decrypt(item.id),
                name = item.name,
                ingredients = item.ingredients,
                instructions = item.instructions,
                category = category,
                user_id = _currentSession.user
            });

            return item;
        }

        public void Remove(string id)
        {
            var recipeId = CryptoEngine.Decrypt(id);
            Expression<Func<Recipe, bool>> getRecipe = recipe => recipe.id == recipeId;
            Recipe currentRecipe = _repositoryManager.RecipeRepository.Get(getRecipe);
            //remove images by recipe
            _repositoryManager.RecipeImageRepository.RemoveByRecipe(recipeId);
            //remove recipe
            _repositoryManager.RecipeRepository.Remove(currentRecipe);
        }
    }
}
