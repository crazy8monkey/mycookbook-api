﻿using System;
using Domain.Repositories;
using Service.Abstractions;

namespace Service
{
    public class ServiceManager : IServiceManager
    {
        private readonly Lazy<IRecipeService> _lazyRecipeService;
        private readonly Lazy<ICategoryService> _lazyCategoryService;
        private readonly Lazy<IUserService> _lazyUserService;
        private readonly Lazy<IErrorCategoryService> _lazyErrorCategoryService;
        private readonly Lazy<IErrorService> _lazyErrorService;
        private readonly Lazy<IRecipeImageService> _lazyRecipeImageService;
        private readonly Lazy<IProfileService> _lazyProfileService;
        private readonly Lazy<ITokenService> _lazyTokenService;
        private readonly Lazy<ILoginService> _lazyLoginService;
        private readonly Lazy<IPasswordService> _lazyPasswordService;
        private readonly Lazy<ISignupService> _lazySignupService;

        public ServiceManager(IRepositoryManager repositoryManager)
        {
            _lazyRecipeService = new Lazy<IRecipeService>(() => new RecipeService(repositoryManager));
            _lazyCategoryService = new Lazy<ICategoryService>(() => new CategoryService(repositoryManager));
            _lazyUserService = new Lazy<IUserService>(() => new UserService(repositoryManager));
            _lazyErrorCategoryService = new Lazy<IErrorCategoryService>(() => new ErrorCategoryService(repositoryManager));
            _lazyErrorService = new Lazy<IErrorService>(() => new ErrorService(repositoryManager));
            _lazyRecipeImageService = new Lazy<IRecipeImageService>(() => new RecipeImageService(repositoryManager));
            _lazyProfileService = new Lazy<IProfileService>(() => new ProfileService(repositoryManager));
            _lazyTokenService = new Lazy<ITokenService>(() => new TokenService(repositoryManager));
            _lazyLoginService = new Lazy<ILoginService>(() => new LoginService(repositoryManager));
            _lazyPasswordService = new Lazy<IPasswordService>(() => new PasswordService(repositoryManager));
            _lazySignupService = new Lazy<ISignupService>(() => new SignupService(repositoryManager));
        }

        public IRecipeService RecipeService => _lazyRecipeService.Value;
        public ICategoryService CategoryService => _lazyCategoryService.Value;
        public IUserService UserService => _lazyUserService.Value;
        public IErrorCategoryService ErrorCategoryService => _lazyErrorCategoryService.Value;
        public IErrorService ErrorService => _lazyErrorService.Value;
        public IRecipeImageService RecipeImageService => _lazyRecipeImageService.Value;
        public IProfileService ProfileService => _lazyProfileService.Value;
        public ITokenService TokenService => _lazyTokenService.Value;
        public ILoginService LoginService => _lazyLoginService.Value;
        public IPasswordService PasswordService => _lazyPasswordService.Value;
        public ISignupService SignupService => _lazySignupService.Value;
    }
}
