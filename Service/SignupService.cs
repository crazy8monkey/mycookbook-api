﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Contracts;
using Domain.Entities;
using Domain.Repositories;
using Microsoft.AspNetCore.Http;
using Service.Abstractions;

namespace Service
{
    public class SignupService: ISignupService
    {
        private readonly IRepositoryManager _repositoryManager;

        public SignupService(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
        }

        public Boolean EmailRegistered(string email, string role)
        {
            Expression<Func<User, bool>> getUserEmailExpression = u => u.email == email && u.user_role == role;
            var currentUserCheck = _repositoryManager.UserRepository.Get(getUserEmailExpression);
            if (currentUserCheck != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /* unused methods */
        public RecipeDTO Add(RecipeDTO item)
        {
            throw new NotImplementedException();
        }

        public RecipeDTO Get(string id)
        {
            throw new NotImplementedException();
        }

        public List<RecipeDTO> List()
        {
            throw new NotImplementedException();
        }

        public void Remove(string id)
        {
            throw new NotImplementedException();
        }

        public void setRequest(HttpRequest request)
        {
            throw new NotImplementedException();
        }

        public RecipeDTO Update(RecipeDTO item)
        {
            throw new NotImplementedException();
        }
    }
}
