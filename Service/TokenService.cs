﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Contracts;
using Domain.Entities;
using Domain.Repositories;
using Libraries;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using Service.Abstractions;

namespace Service
{
    public class TokenService : ITokenService
    {
        private readonly IRepositoryManager _repositoryManager;
        private JWTTokenHandler jwtToken = new JWTTokenHandler();
        private CurrentSessionDTO _currentSession;
        private HttpRequest _request;

        public TokenService(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
            _currentSession = new CurrentSessionDTO();
        }

        public void setRequest(HttpRequest request)
        {
            _request = request;
            _currentSession = request.HttpContext.Features.Get<CurrentSessionDTO>();
        }

        public object NewToken(RefreshTokenDTO refreshToken)
        {
            Expression<Func<User, bool>> getUserIdRefreshToken = (u => u.id == _currentSession.user
                && u.refresh_token == refreshToken.refresh_token
                && u.user_role == _currentSession.role
            );

            User currentUser = _repositoryManager.UserRepository.Get(getUserIdRefreshToken);
            currentUser.refresh_token = jwtToken.RefreshToken();
            _repositoryManager.UserRepository.Update(currentUser);

            dynamic updatedUser = new System.Dynamic.ExpandoObject();
            updatedUser.token = jwtToken.GenerateToken(CryptoEngine.Encrypt(_currentSession.user), _currentSession.role);
            updatedUser.refresh_token = currentUser.refresh_token;
            return updatedUser;

        }

        public bool ValidToken(RefreshTokenDTO refreshToken)
        {
            string authHeader = _request.HttpContext.Request.Headers[HeaderNames.Authorization];
            string token = authHeader.Replace("Bearer ", "");

            var validToken = jwtToken.GetPrincipalFromExpiredToken(token);

            Expression<Func<User, bool>> getUser = u => u.id == _currentSession.user && u.user_role == _currentSession.role;
            User currentUser = _repositoryManager.UserRepository.Get(getUser);

            if (validToken != null ||
                currentUser != null ||
                currentUser.refresh_token == refreshToken.refresh_token || currentUser.refresh_token_exp >= DateTime.Now)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string GenerateToken(string id, string role)
        {
            return jwtToken.GenerateToken(id, role);
        }

        /* unused methods */
        public UserDTO Add(UserDTO item)
        {
            throw new NotImplementedException();
        }

        public UserDTO Get(string id)
        {
            throw new NotImplementedException();
        }

        public List<UserDTO> List()
        {
            throw new NotImplementedException();
        }

        public void Remove(string id)
        {
            throw new NotImplementedException();
        }

        public UserDTO Update(UserDTO item)
        {
            throw new NotImplementedException();
        }
    }
}
