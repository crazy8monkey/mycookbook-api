﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using Contracts;
using Domain.Entities;
using Domain.Repositories;
using Libraries;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using Service.Abstractions;

namespace Service
{
    public class UserService : IUserService
    {
        private readonly IRepositoryManager _repositoryManager;
        private JWTTokenHandler jwtToken = new JWTTokenHandler();
        private CurrentSessionDTO _currentSession;
        private HttpRequest _request;

        public UserService(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
            _currentSession = new CurrentSessionDTO();
        }

        public void setRequest(HttpRequest request)
        {
            _request = request;
            _currentSession = request.HttpContext.Features.Get<CurrentSessionDTO>();
        }

        public UserDTO Add(UserDTO item)
        {
            byte[] newSalt = CryptoEngine.GenerateSalt();
            byte[] userPassword = CryptoEngine.ComputeHash(item.confirm_password, newSalt);

            User newUser = _repositoryManager.UserRepository.Add(new User
            {
                first_name = item.first_name,
                last_name = item.last_name,
                email = item.email,
                registered_date = DateTime.UtcNow,
                password = userPassword,
                salt = newSalt,
                refresh_token = jwtToken.RefreshToken(),
                user_role = item.user_role
            });
            item.id = CryptoEngine.Encrypt(newUser.id);
            item.refresh_token = newUser.refresh_token;
            return item;
        }

        public UserDTO Get(string id)
        {
            var userId = CryptoEngine.Decrypt(id);
            Expression<Func<User, bool>> getUserExpression = user => user.id == userId;
            
            var currentUser = _repositoryManager.UserRepository.Get(getUserExpression);
            return new UserDTO
            {
                id = id,
                first_name = currentUser.first_name,
                last_name = currentUser.last_name,
                email = currentUser.email,
                registered_date = String.Format("{0} {1}", currentUser.registered_date.ToShortDateString(), currentUser.registered_date.ToShortTimeString())
            };
        }

        public List<UserDTO> List()
        {

            Expression<Func<User, bool>> getAppUsers = user => user.user_role == "User";
            var currentUsers = _repositoryManager.UserRepository.List(getAppUsers);

            return currentUsers.Select(user => new UserDTO
            {
                id = CryptoEngine.Encrypt(user.id),
                first_name = user.first_name,
                last_name = user.last_name,
                email = user.email,
                registered_date = String.Format("{0} {1}", user.registered_date.ToShortDateString(), user.registered_date.ToShortTimeString())
            }).ToList();
        }

        public void Remove(string id)
        {
            var userId = CryptoEngine.Decrypt(id);

            //remove user recipes
            Expression<Func<Recipe, bool>> getUserRecipes = recipe => recipe.user_id == userId;
            List<Recipe> userRecipes = _repositoryManager.RecipeRepository.List(getUserRecipes);
            _repositoryManager.RecipeRepository.RemoveByRange(userRecipes);
            //RemoveByRange

            Expression<Func<Category, bool>> getUserCategories = category => category.user_id == userId;
            List<Category> userCategories = _repositoryManager.CategoryRepository.List(getUserCategories);
            _repositoryManager.CategoryRepository.RemoveByRange(userCategories);

            //remove user
            Expression<Func<User, bool>> getUser = user => user.id == userId;
            User currentUser = _repositoryManager.UserRepository.Get(getUser);
            _repositoryManager.UserRepository.Remove(currentUser);
        }

        public UserDTO Update(UserDTO item)
        {
            throw new NotImplementedException();
        }
    }
}
