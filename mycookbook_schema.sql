CREATE DATABASE mycookbook;

CREATE TABLE recipes (
  id BIGINT IDENTITY(1,1),
  name VARCHAR(500),
  ingredients TEXT,
  instructions TEXT,
  category BIGINT,
  submitted_date DATETIME,
  user_id BIGINT
);

CREATE TABLE categories (
  id BIGINT IDENTITY(1,1),
  name VARCHAR(500),
  user_id BIGINT
);

CREATE TABLE categories_error (
  id BIGINT IDENTITY(1,1),
  name VARCHAR(500)
);

CREATE TABLE errors (
  id BIGINT IDENTITY(1,1),
  first_name VARCHAR(100) NULL,
  last_name VARCHAR(100) NULL,
  user_id BIGINT NULL,
  description TEXT,
  category BIGINT,
  submitted_date DATETIME
);

CREATE TABLE recipe_images (
  id BIGINT IDENTITY(1,1),
  name VARCHAR(MAX) NULL,
  picture VARBINARY(MAX) NULL,
  picture_type VARCHAR(MAX) NULL,
  recipe BIGINT,
  size BIGINT,
  is_primary BIT
);

CREATE TABLE users (
  id BIGINT IDENTITY(1,1) NOT NULL,
  first_name VARCHAR(200),
  last_name VARCHAR(200),
  email VARCHAR(255) UNIQUE,
  registered_date DATETIME,
  password VARBINARY(MAX),
  salt VARBINARY(MAX),
  refresh_token VARCHAR(255) NULL,
  profile_pic VARBINARY(MAX) NULL,
  profile_pic_type VARCHAR(MAX) NULL,
  reset_password_exp DATETIME,
  refresh_token_exp DATETIME,
);
